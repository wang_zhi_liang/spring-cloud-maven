package io.springboot.sericefeign.service.impl;

import io.springboot.sericefeign.service.IHelloService;
import org.springframework.stereotype.Component;

@Component
public class HelloServerImpl implements IHelloService {
    @Override
    public String hello(String params) {
        return "服务出现问题，由熔断程序返回错误结构";
    }
}
