package io.springboot.eurekaclient.controller;

import io.springboot.eurekaclient.entity.Msg;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Home {

    @Value("${server.port}")
    public Integer serverPort;

    @Value("${spring.application.name}")
    public String applicationName;

    @RequestMapping("/hello")
    public Msg home(String params) {
        Msg msg = new Msg();
        msg.setCode(1);
        msg.setMessage("applicationName=" + applicationName + ",serverPort=" + serverPort + ",params=" + params);
        return msg;
    }
}
