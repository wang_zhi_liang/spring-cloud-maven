package io.springboot.eurekaclient.entity;

import lombok.Data;

@Data
public class Msg {
    private String message;
    private Integer code;

}
