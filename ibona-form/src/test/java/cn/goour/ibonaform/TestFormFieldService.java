/*
package cn.goour.ibonaform;

import cn.goour.bean.SearchFormField;
import cn.goour.entity.FormFieldDO;
import cn.goour.service.FormFieldService;
import cn.goour.service.FormInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestFormFieldService {
    @Autowired
    FormFieldService fieldService;
    @Autowired
    FormInfoService infoService;

    @Test
    public void testList(){
        SearchFormField searchFormInfo = new SearchFormField();
        List<FormFieldDO> list = fieldService.list(searchFormInfo,3);
        System.out.println("====== 获取所有列表 ======");
        System.out.println(list.getClass().getSimpleName());
        System.out.println(list);
    }
    @Test
    public void testAdd(){
        FormFieldDO field = new FormFieldDO();
        field.setFormId(infoService.list().get(0).getId());
        field.setName(String.format("字段名称，添加%s", format.format(new Date())));
        field.setType("字段类型");
        field.setTime(new Date());
        field.setRequired(true);
        fieldService.add(field);

        testList();
    }
    @Test
    public void testUpdate(){
        FormFieldDO field = fieldService.list().get(0);
        System.out.println(field);
        field.setName(String.format("我在%s更改了字段名称", format.format(new Date())));
        fieldService.update(field);

        testList();
    }
    @Test
    public void testSetRequired(){
        FormFieldDO field = fieldService.list().get(0);
        System.out.println(field);
        fieldService.setRequired(field.getId(), !field.getRequired());

        testList();
    }
    @Test
    public void testSetDisplay(){
        FormFieldDO field = fieldService.list().get(0);
        System.out.println(field);
        fieldService.setDisplay(field.getId(), !field.getDisplay());

        testList();

    }
    public static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}
*/
