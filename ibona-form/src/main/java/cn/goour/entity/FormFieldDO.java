package cn.goour.entity;


import cn.goour.enums.FormFieldTypeEnum;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 表单字段信息
 *
 * @author 侯坤林
 */
@Data
public class FormFieldDO {
    /**
     * 主键，自增
     */
    private Integer id;
    /**
     * 字段名称
     */
    @NotBlank(message = "字段名称不能为空")
    private String name;
    /**
     * 字段类型
     */
    @NotNull(message = "字段类型不能为空")
    private FormFieldTypeEnum type;
    /**
     * 创建时间
     */
    private Date time;
    /**
     * 排序值
     */
    private Integer sort;
    /**
     * 是否必填
     */
    private Boolean required;
    /**
     * 是否显示
     */
    private Boolean display;
    /**
     * 关联表单ID
     */
    private Integer formId;
}
