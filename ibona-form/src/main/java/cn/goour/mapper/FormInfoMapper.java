package cn.goour.mapper;

import cn.goour.dto.SearchInfoDTO;
import cn.goour.entity.FormInfoDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 表单信息 mybatis mapper 接口
 *
 * @author 侯坤林
 */
public interface FormInfoMapper {
    /**
     * 通过主键ID删除一条记录
     *
     * @param formInfoId 表单ID
     */
    void deleteById(@Param("id") Integer formInfoId);

    /**
     * 插入一条表单信息记录
     *
     * @param record 表单信息
     */
    void insert(FormInfoDO record);

    /**
     * 通过主键ID查询一条表单记录
     *
     * @param formInfoId 表单ID
     * @return 表单信息
     */
    FormInfoDO getById(@Param("id") Integer formInfoId);

    /**
     * 通过表单ID做为where条件修改一条记录
     *
     * @param record 表单信息
     */
    void updateById(FormInfoDO record);

    /**
     * 设置表单启用状态
     *
     * @param formInfoId 表单ID
     * @param isEnabled  是否启用
     */
    void setEnabled(@Param("id") Integer formInfoId, @Param("enabled") Boolean isEnabled);

    /**
     * 查询表单信息列表
     *
     * @param searchInfoDTO 查询信息
     * @return 表单列表
     */
    List<FormInfoDO> listFormInfo(SearchInfoDTO searchInfoDTO);
}