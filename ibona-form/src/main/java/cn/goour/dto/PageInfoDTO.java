package cn.goour.dto;

import lombok.Data;

/**
 * 从客户的传来的分页参数
 *
 * @author 侯坤林
 */
@Data
public class PageInfoDTO {
    /**
     * 列表分页，页码
     */
    private Integer page = 1;
    /**
     * 列表分页，每页显示条数
     */
    private Integer limit = 10;
}
