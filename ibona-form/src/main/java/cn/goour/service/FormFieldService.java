package cn.goour.service;

import cn.goour.dto.PageInfoDTO;
import cn.goour.dto.SearchInfoDTO;
import cn.goour.entity.FormFieldDO;
import com.github.pagehelper.PageInfo;

/**
 * 表单字段业务处理接口
 *
 * @author 侯坤林
 */
public interface FormFieldService {
    /**
     * 获取某个表单下的字段列表信息
     *
     * @param searchInfoDTO 查询条件
     * @param pageInfoDTO   分页条件
     * @param formInfoId    表单ID
     * @return 分页列表
     */
    PageInfo<FormFieldDO> list(SearchInfoDTO searchInfoDTO, PageInfoDTO pageInfoDTO, Integer formInfoId);

    /**
     * 修改字段信息
     *
     * @param formFieldDO 字段信息
     */
    void update(FormFieldDO formFieldDO);

    /**
     * 删除字段信息
     *
     * @param formFieldId 字段ID
     */
    void delete(Integer formFieldId);

    /**
     * 添加字段信息
     *
     * @param formFieldDO 字段信息
     */
    void insert(FormFieldDO formFieldDO);

    /**
     * 设置字段是否必填
     *
     * @param formFieldId 字段ID
     * @param isRequired  是否必填
     */
    void setRequired(Integer formFieldId, Boolean isRequired);

    /**
     * 设置字段是否显示
     *
     * @param formFieldId 字段ID
     * @param isDisplay   是否显示
     */
    void setDisplay(Integer formFieldId, Boolean isDisplay);

    /**
     * 通过ID查找一条记录
     *
     * @param formFieldId 字段ID
     * @return 字段信息
     */
    FormFieldDO getById(Integer formFieldId);
}
