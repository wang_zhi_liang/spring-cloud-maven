package cn.goour.utils;

import cn.goour.dto.FormFieldDTO;
import cn.goour.entity.FormFieldDO;

/**
 * 表单字段信息DO、DTO的转换工具
 *
 * @author 侯坤林
 */
public class FormFieldUtils {
    /**
     * 把表单字段信息数据传输对象DTO转换成数据对象DO。
     * 主要用在controller层，把前端传来的数据转换成数据库需要存储的数据
     *
     * @param dto 表单字段数据传输对象DTO
     * @return 表单字段信息数据对象DO
     */
    public static FormFieldDO toFormFieldDO(FormFieldDTO dto) {
        FormFieldDO fieldDO = new FormFieldDO();
        fieldDO.setId(dto.getId());
        fieldDO.setName(dto.getName());
        fieldDO.setType(dto.getType());
        fieldDO.setTime(null);
        fieldDO.setSort(dto.getSort());
        fieldDO.setRequired(dto.getRequired());
        fieldDO.setDisplay(dto.getDisplay());
        fieldDO.setFormId(dto.getFormId());

        return fieldDO;
    }

    /**
     * 把表单字段信息数据对象DO转换成传输对象DTO。
     * 主要用在controller层，把service层返回controller层的数据转换成需要传输给前端的数据。
     *
     * @param fieldDO 表单字段信息数据对象DO
     * @return 表单字段数据传输对象DTO
     */
    public static FormFieldDTO toFormFieldDTO(FormFieldDO fieldDO) {
        FormFieldDTO dto = new FormFieldDTO();
        dto.setId(fieldDO.getId());
        dto.setName(fieldDO.getName());
        dto.setType(fieldDO.getType());
        dto.setTypeText(fieldDO.getType().getText());
        dto.setTime(fieldDO.getTime());
        dto.setSort(fieldDO.getSort());
        dto.setRequired(fieldDO.getRequired());
        dto.setDisplay(fieldDO.getDisplay());
        dto.setFormId(fieldDO.getFormId());
        System.out.println(fieldDO);
        System.out.println(dto);
        return dto;
    }
}
