package cn.goour.utils;

import cn.goour.dto.FormInfoDTO;
import cn.goour.entity.FormInfoDO;

/**
 * 表单DO、DTO的转换工具
 *
 * @author 侯坤林
 */
public class FormInfoUtils {
    /**
     * 把表单信息数据传输对象DTO转换成数据对象DO。
     * 主要用在controller层，把前端传来的数据转换成数据库需要存储的数据
     *
     * @param infoDTO 表单数据传输对象DTO
     * @return 表单信息数据对象DO
     */
    public static FormInfoDO toFormInfoDO(FormInfoDTO infoDTO) {
        FormInfoDO formInfoDO = new FormInfoDO();
        formInfoDO.setId(infoDTO.getId());
        formInfoDO.setName(infoDTO.getName());
        formInfoDO.setTime(null);
        formInfoDO.setSort(infoDTO.getSort());
        formInfoDO.setEnabled(infoDTO.getEnabled());
        formInfoDO.setRemark(infoDTO.getRemark());

        return formInfoDO;
    }

    /**
     * 把表单信息数据对象DO转换成传输对象DTO。
     * 主要用在controller层，把service层返回controller层的数据转换成需要传输给前端的数据。
     *
     * @param infoDO 表单信息数据对象DO
     * @return 表单数据传输对象DTO
     */
    public static FormInfoDTO toFormInfoDTO(FormInfoDO infoDO) {
        FormInfoDTO dto = new FormInfoDTO();
        dto.setId(infoDO.getId());
        dto.setName(infoDO.getName());
        dto.setTime(infoDO.getTime());
        dto.setSort(infoDO.getSort());
        dto.setEnabled(infoDO.getEnabled());
        dto.setRemark(infoDO.getRemark());
        return dto;
    }
}
