package cn.goour.handler;

import cn.goour.dto.BackJsonDTO;
import cn.goour.exception.BackJsonException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolationException;
import javax.validation.UnexpectedTypeException;
import javax.validation.ValidationException;

/**
 * 统一异常捕获处理
 *
 * @author 侯坤林
 */
@ControllerAdvice
public class MyExceptionHandler {

    /**
     * 捕获所有异常信息
     *
     * @param e 异常
     * @return 统一JSON数据返回对象
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public BackJsonDTO handlerAllException(Exception e) {
        e.printStackTrace();
        return new BackJsonDTO(1, e.getMessage());
    }

    /**
     * 捕获手动抛出异常信息
     *
     * @param e 手动抛出异常对象
     * @return 统一JSON数据返回对象
     */
    @ExceptionHandler(BackJsonException.class)
    @ResponseBody
    public BackJsonDTO handlerBackJsonException(BackJsonException e) {
        e.printStackTrace();
        return new BackJsonDTO(1, e.getMessage());
    }

    /**
     * 捕获访问不存在的http方法（GET、POST、PUT、DELETE）时抛出异常
     *
     * @param e 异常
     * @return 统一JSON数据返回对象
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    public BackJsonDTO handlerHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        e.printStackTrace();
        return new BackJsonDTO(1, String.format("不支持%s请求", e.getMethod()));
    }

    /**
     * 从另一个地方搬过来的，好像是数据库增改操作校验失败抛出的异常信息
     *
     * @param e 异常
     * @return 统一JSON数据返回对象
     */
    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    public BackJsonDTO handlerValidationException(ValidationException e) {
        e.printStackTrace();
        String msg = null;
        if (e instanceof ConstraintViolationException) {
            StringBuffer stringBuffer = new StringBuffer("请求参数校验失败");
            ((ConstraintViolationException) e).getConstraintViolations().forEach(it -> {
                stringBuffer.append(it.getMessage());
                stringBuffer.append(",");
            });
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            msg = stringBuffer.toString();

        } else if (e instanceof UnexpectedTypeException) {
            msg = "参数类型转换失败|参数类型不正确|此项一般为程序错误，请联系管理员和复现错误";
        } else {
            msg = "参数验证其他未知错误，请检查参数是否合法和类型是否正确";
        }
        return new BackJsonDTO(1, msg);
    }
}
