package io.springboot.serviceribbon.service;

import com.netflix.ribbon.proxy.annotation.Hystrix;
import io.springboot.serviceribbon.handler.HystrixFallbackHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HelloService {
    @Autowired
    public RestTemplate restTemplate;
/*
    @Hystrix(fallbackHandler = HystrixFallbackHandler.class)*/
    public String get(String params){
        return restTemplate.getForObject("http://SERVICE-HI/hello?params="+params,String.class);
    }
}
